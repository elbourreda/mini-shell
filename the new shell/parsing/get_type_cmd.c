/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_type_cmd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 09:46:18 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 12:19:38 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char** get_type_cmnd(char *line) 
{
    int i;
	char **ret;
	char *type;
	
    // // type = strdup("\0");
	// i = 0;
    // while (line[i] && line[i] == ' ')
    //     i++;

	ret = ft_split_plus(line, ' ');

	return (ret);
}

char* type_ifred_else(char **str, char *type, char **split)
{
	int i;
    int j;

	i = 0;
	j = 1;
    while (str[j])
    {
        i = 0;
        while (str[j] && (str[j][i] == '<' || str[j][i] == '>' || str[j][i] == ' '))
            i++;
        int len = (1 + len_wrds((str[j] + i), ' '));
        if (len > 1)
        {
            split = edited_Split_typecmd(str[j], ' ');
            type = split[2];
            break;
        }
        else
            j++;
    }
	return (type);
}

char* get_type_cmnd_ifred(char *line, char **str, int nbr)
{
    int  i ;
	char **tmp;
    char *type ;
    char **split;
	
	i = 0;
	type = strdup("\0");
	split = NULL;
    while (line && line[i] == ' ')
        i++;
    if (line[i] != '<' && line[i] != '>')
	{
		tmp = get_type_cmnd(line);
		type = tmp[0];
	}
    else
		type = type_ifred_else(str, type, split);
    return (type);
}