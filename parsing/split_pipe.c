/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_pipe.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 17:44:44 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/18 04:11:45 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

int ifsplitis(char *s)
{
    int i;

    i = 0;
    while (s[i] != '\0')
    {
        if (s[i] == '|')
            return 1;
        i++;
    }
    return (0);
}

int skip_s_sqts(int i, char *s)
{
    int j;

	j = 0;
    while (s[i] && s[i] != '\'')
        i++;
    return i;
}

int skip_d_sqts(int i, char *s)
{
    int j;

	j = 0;
    while (s[i] && s[i] != '"')
        i++;
    return i;
}

char **more_splt_allc(int j, int *indexs, int *lenght)
{
	int i;
	char **str;

	i = 0;
	str = (char **)malloc(sizeof(char *) *( j + 1));
	while (i < j)
    {
        if (i > 0)
            lenght[i] = indexs[i] - indexs[i - 1];
        str[i] = (char *)malloc(sizeof(char) * 1024);
        i++;
    }
	return (str);
}

char **more_splt_half(int j, char **str, int *lenght, int *indexs)
{
	t_cmds *data;
	int y;
    int d;
    int i;

	d = 0;
	i = 0;
	data = init_stuct();
	while (i < j)
    {
        y = 0;
        while (y < lenght[i] - 1)
        {
            str[i][y] = data->line[d];
            y++;
            d++;
        }
        str[i][y] = '\0';
        d = 1 + indexs[i];
        i++;
        
    }
    str[i] = NULL;
	return (str);
}

char **more_splt(int *indexs, int j)
{
    t_cmds *data;
	char **str;
    int *lenght;
	
	lenght = (int *)malloc(sizeof(int) * j);
    data = init_stuct();
    lenght[0] = indexs[0] + 1;
	str = more_splt_allc(j, indexs, lenght);
	str = more_splt_half(j, str, lenght, indexs);
    free(lenght);
	lenght = 0;
    return (str);
}

char **split_one_cmnd()
{
    t_cmds *data;
    char **str;

	str = (char **)malloc(sizeof(char *) * 3);
    data = init_stuct();
    str[0] = strdup(data->line);
    str[1] = NULL;
    return (str);
}

char **split_line_pipe_if(char *s, int *indexs, int j, char **str)
{
	int i;
	
	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == '"')
			i = skip_d_sqts(++i, s);
		else if (s[i] == '\'')
			i = skip_s_sqts(++i, s);
		else if (s[i] == '|')
		{
			indexs[j] = i;
			j++;
		}
		i++;
	}
	indexs[j] = i;
	j++;
	if (j)
		str = more_splt(indexs, j);
	return (str);
}

int get_lenght_by_char(char *s, char c)
{
	int i;
	int len;

	len = 0;
	i = 0;
	while (s[i])
	{
		if (s[i] == '"')
			i = skip_d_sqts(++i, s);
		else if (s[i] == '\'')
			i = skip_s_sqts(++i, s);
		else if (s[i] == c)
			len++;
		i++;
	}
	return (len);
}

char **split_line_pipe(char *s)
{
    int i;
	int j;
    int *indexs;
    char **str;

	str = NULL;
	int pipe_len;

	pipe_len = get_lenght_by_char(s, '|');
    indexs = (int *)malloc(sizeof(int ) * (pipe_len + 1));
	// indexs = (int *)malloc(sizeof(int ) * (100 + 1));
    i = 0;
	j = 0;
	if (pipe_len > 0)
		str = split_line_pipe_if(s,  indexs,  j, str);
    else
        str = split_one_cmnd();
    free(indexs);
    indexs = 0;
    return (str);
}
