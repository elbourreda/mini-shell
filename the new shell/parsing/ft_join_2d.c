/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_join_2d.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 09:43:47 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 12:41:26 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "../header.h"

int len_array(char **args)
{
    int i = 0;
    while (args && args[i])
		i++;  
    return i;
}

char **join2d(char **args, char *s)
{   
    char **ret;
    int i;
	
	
    ret = (char **)malloc(sizeof(char *) * (len_array(args) + 2));
    i = 0;
    while (i < (len_array(args)))
    {
        ret[i] = strdup(args[i]);
        i++;
    }
	if (!s)
		ret[i] = strdup("");
	else
		ret[i] = strdup(s);
    i++;
    ret[i] = NULL;
	free_array(args);
    return (ret);
}
