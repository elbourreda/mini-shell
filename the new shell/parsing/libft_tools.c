/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 08:40:28 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/14 23:55:41 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"


char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*dst;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	if (start > strlen(s))
		len = 0;
	dst = (char *)malloc(len + 1);
	if (dst == NULL)
		return (NULL);
	while (i < len && s[i] != '\0')
	{
		dst[i] = s[start];
		start++;
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

int		ft_isalpha(int c)
{
	if ((c > 64 && c < 91) || (c > 96 && c <= 122))
		return (1);
	return (0);
}

char	*ft_strcpy(char *dest, const char *src)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	size_s1;
	char	*strjoin;

	if (!s1 || !s2)
		return (NULL);
	size_s1 = strlen(s1);
	strjoin = (char*)malloc(sizeof(char) * (strlen(s1) + strlen(s2) + 1));
	if (&strjoin[0] == 0)
		return (0);
	ft_strcpy(&strjoin[0], s1);
	ft_strcpy(&strjoin[size_s1], s2);
	return (strjoin);
}

int	ft_isprint(int c)
{
	if (c > 32 && c <= 126)
		return (1);
	else
		return (0);
}

int		ft_isalnum(int c)
{
	if (c >= 48 && c <= 57)
		return (1);
	else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return (1);
	else
		return (0);
}