/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 21:58:28 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/16 03:41:09 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char *addqouts(char *s)
{
	int i;
	char *ret;

	i = 0;
	ret = strdup("");
	char *tmp = ret;
	ret = strjoin1(ret, '\'');
	free(tmp);
	while (s[i])
	{
		if (s[i] == ' ')
		{
			tmp = ret;
			ret = strjoin1(ret, '\'');
			free(tmp);
			
			tmp = ret;
			ret = strjoin1(ret, s[i]);
			free(tmp);
			
			tmp = ret;
			ret = strjoin1(ret, '\'');
			free(tmp);
		}
		else
		{
			tmp = ret;
			ret = strjoin1(ret, s[i]);
			free(tmp);
		}
		i++;
	}
	tmp = ret;
	ret = strjoin1(ret, '\'');
	free(tmp);
	return ret;
}
char *status_env_ret_qts(char *tmp, char *str, int j)
{
	char *temp = tmp;
	tmp = ft_strjoin(tmp, "0");
	free(temp);
	
	char *s = ft_substr(str, (j), strlen((str + (j))));
	
	temp = tmp;
	tmp = ft_strjoin(tmp, s);
	free(temp);
	
	free(str);
	free(s);
	return (tmp);
}
char *change_env_half_qots(char *str, int *j, int j2)
{
	char *ret = strdup("");
	(*j)--;
	char *tmp = ft_substr(str, 0, (*j));
	(*j)++;
	
	if (str[*j] && (ft_isalpha(str[*j]) || str[*j] == '_') )
	{
		while (str[*j] && (ft_isalnum(str[*j]) || str[*j] == '_'))
		{
			char *temp = ret;
			ret = strjoin1(ret, str[*j]);
			free(temp);
			(*j)++;
		} 
	}
	else if (str[*j] && str[*j] == '?')
	{
		(*j)++;
		str = status_env_ret_qts(tmp, str, *j);
		free(ret);
		(*j) = (j2);
		return (str);
	}
	char *data = getenv(ret);
	if (!data)
		data = "";

	char *temp = tmp;
	tmp = ft_strjoin(tmp, data);
	free(temp);

	
	char *s = ft_substr(str, (*j), strlen((str + (*j))));
	
	temp = tmp;
	tmp = ft_strjoin(tmp, s);
	free(temp);
	// // str = tmp;
	(*j) = (j2 + (strlen(data)));
	
	free(ret);
	free(s);
	free(str);
	return (tmp);
}

char * change_env_dqots(char *str, int *j)
{
	(*j)++; 
	while (str[*j] && str[*j] != '\"')
	{
		if (str[*j] == '$')
		{
			int j2 = (*j);
			while (str[*j] == '$')
				(*j)++;
			if (str[*j] && str[*j] == '?')
			{
				str = change_env_half_qots(str, j, j2);
			}
			else if ((!str[*j] || (!ft_isalpha(str[*j]) && str[*j] != '_') 
			||  (str[*j] && !ft_isprint(str[*j]))))
			{
				(*j)++;
			}
			else
				str = change_env_half_qots(str, j, j2);
		}
		else
			(*j)++;
	}
	return (str);
}

char *status_env_ret(char *tmp, char *str, int j)
{
	char *temp = tmp;
	tmp = ft_strjoin(tmp, "0");
	free(temp);
	char *s = ft_substr(str, (j), strlen((str + (j))));
	temp = tmp;
	tmp = ft_strjoin(tmp, s);
	free(temp);
	free(str);
	free(s);
	return (tmp);
}

char *change_env_no_qots(char *str, int *j, int j2)
{
	char *ret = strdup("");
	(*j)--;
	char *tmp = ft_substr(str, 0, (*j));
	(*j)++;
	if (str[*j] && (ft_isalpha(str[*j]) || str[*j] == '_') )
	{
		while (str[*j] && (ft_isalnum(str[*j]) || str[*j] == '_'))
		{
			char *temp = ret;
			ret = strjoin1(ret, str[*j]);
			free(temp);
			(*j)++;
		} 
	}
	else if (str[*j] && str[*j] == '?')
	{
		(*j)++;
		
		str = status_env_ret(tmp, str, *j);
		free(ret);
		(*j) = (j2);
		return (str);
	}
	char *data = getenv(ret);
	int data_alloc = 0;
	if (!data)
		data = "";
	else
	{
		data = addqouts(data);
		data_alloc = 1;
	}
	char *temp = tmp;
	tmp = ft_strjoin(tmp, data);
	free(temp);
	
	char *s = ft_substr(str, (*j), strlen((str + (*j))));

	temp = tmp;
	tmp = ft_strjoin(tmp, s);
	free(temp);
	// str = tmp;
	(*j) = (j2 + (strlen(data)) - 1);
	
	free(ret);
	if (data_alloc == 1)
		free(data);
	free(s);
	free(str);
	
	// return (str);

	return tmp;
}


char *change_env_while(char *str, int j)
{
	int j2;

	while (str[j])
	{
		if (str[j] == '\'')
			j = skip_s_sqts(++j, str);
		else if (str[j] == '\"')
			str = change_env_dqots(str ,&j);
		else if (str[j] == '$')
		{
			j2 = j;
			while (str[j] == '$')
				j++;
			if (str[j] && str[j] == '?')
			{
				str = change_env_no_qots(str, &j, j2);
			}
			else if (!str[j] || (!ft_isalpha(str[j]) &&
			str[j] != '_') || (str[j] && !ft_isprint(str[j])))
			{
				j++;
				continue;
			}
			else
				str = change_env_no_qots(str, &j, j2);
		}
		j++;
	}
	return (str);
}

char **change_env(char **str)
{
	int i;
	int j;

	i = 0;
	while (str[i])
	{
		j = 0;
		str[i] = change_env_while(str[i], j);
		i++;
	}
	return str;
}
