/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delet_space.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/18 23:35:42 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 13:07:38 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"


char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char			*res;
	unsigned int	i;

	if (!s)
		return (0);
	if (!(res = (char *)malloc(sizeof(*s) * (len + 1))))
		return (NULL);
	i = 0;
	while (s[start + i] && i < len)
	{
		res[i] = s[start + i];
		i++;
	}
	res[i] = '\0';
	return (res);
}


char	*ft_strnew(size_t size)
{
	char	*res;
	size_t	i;

	if (!(res = (char *)malloc(sizeof(char) * size + 1)))
		return (NULL);
	i = 0;
	while (i < size)
		res[i++] = '\0';
	res[i] = '\0';
	return (res);
}


static int		ft_get_start(const char *s)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (s[i] != ' ' && s[i] != '\t' && s[i] != '\n')
			return (i);
		i++;
	}
	return (-1);
}

static int		ft_get_end(const char *s)
{
	int i;

	i = strlen(s) - 1;
	while (i > 0)
	{
		if (s[i] != ' ' && s[i] != '\t' && s[i] != '\n')
			return (i);
		i--;
	}
	return (-1);
}

// char			*ft_strtrim(char const *s)
// {
// 	int		start;
// 	int		end;
// 	char	*res;

// 	if (!s)
// 		return (NULL);
// 	start = ft_get_start(s);
// 	end = ft_get_end(s);
// 	if (end == -1 || start == -1)
// 		return (ft_strnew(0));
// 	res = ft_strsub(s, start, end - start + 1);
// 	return (res);
// }

char *ft_split_half_dbl_qots(int *i, int j, char *str, char **ret, char c)
{
	(*i)++;
	while (str[*i] && str[*i] != c)
	{
		char *tmp = ret[j];
		ret[j] =  strjoin1(ret[j], str[*i]);
		free(tmp);
		(*i)++;
	}
	(*i)++;
	return (ret[j]);
}

int len_wrds_edit(char *str, char c)
{
    int i;
    int len;

	if (!str)
		return (0);
    i = 0;
    len = 0;
	// while (str[i] && str[i] == c)
	// 	i++;
	str = ft_strtrim(str, " ");
	i = 0;
	while (str[i])
    {
		if (str[i] != ' ' && str[i] != '<' && str[i] != '>')
		{
			len = len + 1;
			while (str[i] && str[i] != ' ' && str[i] != '<' && str[i] != '>')
			{
				if (str[i] == '\"')
					i = skip_d_sqts(++i, str); 
				else if (str[i] == '\'')
					i = skip_s_sqts(++i, str);
				i++;
			}
		}
		else
			i++;
    }
	free(str);
	str = NULL;
    return (len);
}

char **alloc_ft_split(char **ret, char *str, char c)
{ 
    int i;
    int len;
    
    i = 1;
    len = len_wrds_edit(str, c);
	if (!len)
		len = 1;
    ret = (char **)malloc(sizeof(char *) * (len + 1));
    while (i < (len))
    {
        // ret[i] = (char *)malloc(sizeof(char) * 1);
        i++;
    }
    // ret[i] = (char *)malloc(sizeof(char));
    ret[i] = NULL;
    return (ret);
}

char **ft_split_plus_half(char *str, char c, int i, char **ret, int j)
{
	j = 0;
	while (str[i] != '\0')
    {
		char g = str[i];
		if (str[i] && (str[i] != ' ' && str[i] != '>' && str[i] != '<'))
		{
			while (str[i] && str[i] != ' ' && str[i] != '>' && str[i] != '<')
			{
				if (str[i] == '\"')
				{
					ret[j] = ft_split_half_dbl_qots(&i, j,str, ret, '\"');
					continue;
				}
				else if (str[i] == '\'')
				{
					ret[j] = ft_split_half_dbl_qots(&i, j,str, ret, '\'');
					continue;
				}
				else
				{
					char *tmp = ret[j];
					ret[j] =  strjoin1(ret[j], str[i]);
					free(tmp);
				}
				i++;
			}
		}
		else
		{
			while (str[i] && (str[i] == ' ' || str[i] == '>' || str[i] == '<'))
				i++;
			j++;
			ret[j] = strdup("");
			// ret[j] = "";
		}
    }
	return (ret);
}

char **ft_split_plus(char *str, char c)
{
	
    int i;
    int len;
    char **ret;
    int j;
	if (!str)
		return NULL;
    i = 0;
	j = 0;
	ret = alloc_ft_split(ret, str, c);
	ret[j] = strdup("");
	str = ft_strtrim(str, " ");
	ret = ft_split_plus_half(str, c, i, ret, j);
	free(str);
	str = NULL;
	return (ret);
}