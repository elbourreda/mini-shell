/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_pars.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 17:07:20 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 13:00:52 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

t_cmds *init_stuct()
{
    static t_cmds *data;
	if (!data)
		data = malloc(sizeof(t_cmds));
    return (data);
} 

t_cmds *new_node(char *str)
{
	t_cmds *tmp;

	tmp = (t_cmds *)malloc(sizeof(t_cmds));
	tmp->command = strdup(str);
	tmp->type = NULL;
	// tmp->arguments = (char **)malloc(sizeof(char *));
	// tmp->arguments[0] = NULL;
	tmp->arguments = NULL;
	// tmp->all = (char **)malloc(sizeof(char *));
	// tmp->all[0] = NULL;
	tmp->all = NULL;
	tmp->next_cmd = NULL;
	return (tmp);
}

void join_list(t_cmds *tmp, t_cmds *src)
{
	t_cmds *ret = tmp;
	
	while (ret->next_cmd != NULL)
		ret = ret->next_cmd;
		
	ret->next_cmd = src;
}

void init_linkedlist(char **str)
{
    int i;
    t_cmds *data;
    t_cmds *tmp;
	t_cmds *src;
	
	data = init_stuct();
	tmp = data;
	i = 0;
	while (str[i])
    {
		if (i == 0)
		{
			tmp->command = strdup(str[i]);
			tmp->type = NULL;
			// tmp->arguments = (char **)malloc(sizeof(char *));
			// tmp->arguments[0] = NULL;
			tmp->arguments = NULL;
			// tmp->all = (char **)malloc(sizeof(char *));
			// tmp->all[0] = NULL;
			tmp->all = NULL;
			tmp->next_cmd = NULL;
		}
		else
		{
			src = new_node(str[i]);
			join_list(tmp, src);
		}
		i++;		
	}
}

void clear_list(t_cmds *tmp)
{
	int j;

	j = 0;
	if (tmp->type)
	{
		free(tmp->type);
		tmp->type = NULL;
	}
	if (tmp->command) //cmd
	{
		free(tmp->command);
		tmp->command = NULL;
	}
	while (tmp->arguments[j]) //args
	{
		free(tmp->arguments[j]);
		tmp->arguments[j] = NULL;
		j++;
	}
	free(tmp->arguments);
	tmp->arguments = NULL;
	j = 0;
	while (tmp->all[j]) // all
	{
		free(tmp->all[j]);
		tmp->all[j] = NULL;
		j++;
	}
	free(tmp->all);
	tmp->all = NULL;
	
}

void free_list()
{
	t_cmds *data;

	data = init_stuct();
	int d = 0;
	while (data)
	{
		t_cmds *tmp = data;
		data = data->next_cmd;
		clear_list(tmp);
		if (d != 0)
			free(tmp);
		d++;
	}
}



int start_parsing(char **envs) 
{
    t_cmds *data;
    char **splitline;

    data = init_stuct();

    if (check_emptyline() || syntax_errors(NULL, 2) == -1) // syntax quotes
        return (-1); 
    splitline = split_line_pipe(data->line);
	
    if (syntax_errors(splitline, 1) == -1) // syntax pipes
	{
		free_array(splitline);
		return (-1);
	}

	change_env(splitline);
	
	// for(int h = 0; splitline[h]; h++)
	// 	printf("[%s]\n", splitline[h]);
	
    init_linkedlist(splitline);
	
	free_array(splitline);
	
    if (edit_cmds() == -1) // x red
        return (-1);
    // result
    printf_output(data);

	free_list();
    return (0);
}

int main_parsing(char **envs)
{
    t_cmds *data;

    data = init_stuct();
    data->line = readline("User@minishell> ");
    if (!data->line)
    {
        printf("exit\n");
        exit(0);
    }
    add_history(data->line);
    if (start_parsing(envs) == -1)
    {
        free(data->line);
        data->line = NULL;
        return (-1);
    }
    free(data->line);
    data->line = NULL;
    return (0);
}
