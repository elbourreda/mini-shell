/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 17:06:16 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/13 00:43:07 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>//for exit
#include <unistd.h> // for fork
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/wait.h> // for wait 

// HEADER FILE FOR PARSING
#include "parsing/parse_header.h"


typedef struct s_filerdr
{
    char *all;
    char *name;
    int type;
	
    struct s_filerdr *next_one;
} t_filerdr;

typedef struct s_cmds
{
    char *line; // line kaml
    char *command; // sing command kaml bla pip
	char **all;
    char *type; // command name
    char **arguments;
    int option;
    t_filerdr *redrctions;
    struct s_cmds *next_cmd;
    
} t_cmds;

t_cmds *init_stuct();
int main_parsing(char **envs);

#endif
