/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_args.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 09:42:02 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/19 02:29:13 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char** get_args(char *line)
{
    int i;
	int len;
	char **ret;
	char **tmp;
	
	i = 0;
    while (line[i] && line[i] == ' ')
        i++;
	ret = edited_Split((line + i), ' ');
    len = len_array(ret);
	tmp = (char **)malloc(sizeof(char *) * 1);
    tmp[0] = NULL;
    i = 1;
    if (len > 1)
    {
        while (ret[i])
        {
            tmp = join2d(tmp, ret[i]);
            i++;
        }
    }
    return (tmp);
}

char **ifred_if_half(int y, char **tmp, char **ret, int i, char **str)
{
	int len;
	
	y = 1;
	while (str[y])
	{
		// ret = edited_Split(str[y], ' ');
			// for(int h = 0; splitline[h]; h++)
		// printf("[%d] ====> [%s]\n",y,  str[y]);
	
		ret = edited_Split_typecmd(str[y], ' ');
		// printf("\n\n\n\n\n\n\n\n\n\n\n");
		len = len_array(ret);
		if (len > 2)
		{
			i = 2; 
			while (ret[i])
			{
				tmp = join2d(tmp, ret[i]);
				i++;
			}
		}
		y++;
	}
	return (tmp);
}

char **get_args_ifred_if(int len, char **ret, char **str, char **tmp, int i)
{
	int y;

	if (str[0])
	{
		// ret = edited_Split(str[0], ' ');
		// ret =  edited_Split_typecmd(str[0], ' ');
		ret =  ft_split_plus(str[0], ' ');
		len = len_array(ret);
		if (len > 1)
		{
			i = 1;
			while (i < (len - 1))
			{
				tmp = join2d(tmp, ret[i]);
				i++;
			}
		}
	}
	tmp = ifred_if_half(y, tmp, ret,  i, str);
	return (tmp);
}


char **args_red_else_half(char **tmp, int i, char **ret, int *cmd)
{
	i = 2;
	if ((*cmd) == 0)
	{
		i = i + 1;
		(*cmd) = 1;
	}
	while (ret[i])
	{
		tmp = join2d(tmp, ret[i]);
		i++;
	}
	return (tmp);
}

char **get_args_ifred_else(int y, char **str, int i, char **tmp)
{
	int cmd;
	int len;
	char **ret;
	
	cmd = 0;
	y = 1;
    while (str[y])
    {
        // ret = edited_Split(str[y], ' ');
		ret = edited_Split_typecmd(str[y], ' ');
		
        len = len_array(ret);
        if (len > 2)
			tmp = args_red_else_half(tmp,  i, ret, &cmd);
        y++;
    }
	return (tmp);
}

char** get_args_ifred(char *line, char **str, int j)
{
	char **tmp;
	char **ret;
	int len;
	int i;
	int cmd;
	int y;

    tmp = (char **)malloc(sizeof(char *));
    tmp[0] = NULL;
    i = 0;
    while (line && line[i] == ' ')
        i++;
    if (line[i] != '<' && line[i] != '>')
		tmp = get_args_ifred_if(len, ret, str, tmp, i);
    else
		tmp = get_args_ifred_else(y, str,  i, tmp);
    return (tmp);
}