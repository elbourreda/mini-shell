/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_red.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 08:57:51 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/11 02:33:39 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char* get_filename(char *str)
{
    int j = 0;
    char **ret;
    char *s;

    j = 0;
    while (str[j] && (str[j] == '<' || str[j] == '>' || str[j] == ' '))
        j++;
    ret = edited_Split((str + j), ' ');
    s = ret[0];
    return (s);
}

int get_type_red(char *str)
{
    int j;
    int ret;
    
    j = 0;
    ret = -1;
    while (str[j] && str[j] == ' ')
        j++;
    if (str[j] == '>' && str[j + 1] != '>' )
        ret = 1;
    else if (str[j] == '<' && str[j + 1] != '<' )
        ret = 0;
    else if (str[j] == '>' && str[j + 1] == '>' )
        ret = 2;
    else if (str[j] == '<' && str[j + 1] == '<' )
        ret = 3;
    return (ret);
}
