/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_edited_typ.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 11:30:28 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/18 21:59:26 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "../header.h"

char **alloc_ret_split(char **ret, char *str, char c)
{ 
    int i;
    int len;
    
    i = 0;
    len = len_wrds(str, c);
    ret = (char **)malloc(sizeof(char *) * (len + 2));
    while (i < len + 1)
    {
        ret[i] = (char *)malloc(sizeof(char) * 100);
        i++;
    }
    ret[i] = NULL;
    return (ret);
}


char *split_typecmd_half_dbl_qots(int *i, int j, char *str, char **ret, char c)
{
	(*i)++;
	while (str[*i] && str[*i] != c)
	{
		// char *tmp = ret[j];
		ret[j] =  strjoin1(ret[j], str[*i]);
		// free(tmp);
		(*i)++;
	}
	(*i)++;
	return (ret[j]);
}

char **edited_Split_typecmd_half(char *str, char c, int i, char **ret, int j)
{
	while (str[i] && str[i] == ' ')
		i++;
	while (str[i] != '\0')
    {
		if (str[i] == '\"')
			ret[j] = split_typecmd_half_dbl_qots(&i, j,str, ret, '\"');
		else if (str[i] == '\'')
			ret[j] = split_typecmd_half_dbl_qots(&i, j,str, ret, '\'');
		else if (str[i] == ' ' || str[i] == '>' || str[i] == '<')  /// char c
		{
			i++;
			while (str[i] && (str[i] == ' ' || str[i] == '>' || str[i] == '<'))
				i++;
			// j++;
			// ret[j] = strdup("");
		}
		else
		{
			
			// ret[j] = strdup("");
			// char *tmp = ret[j];
			ret[j] =  strjoin1(ret[j], str[i]);
			j++;
			// j++; // jadi ztha hit segu fach kandil ls bo7dha
			// free(tmp);
			i++;
		}
    }
	// j++;
	// ret[j] = NULL;
	return (ret);
}

char **edited_Split_typecmd(char *str, char c)
{
    int i;
    int len;
    char **ret;
    int j;
	// printf("[%s]\n", str);
	// if (!str)
	// 	str = strdup("");
    ret = alloc_ret_split(ret, str, c);
    i = 0;
    j = 0;
    ret[j] = strdup("");
    str = delete_backspace(str);
    while (str && str[i] == ' ')
        i++;
	return (edited_Split_typecmd_half(str, c, i, ret, j));
}