/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_position.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 09:48:12 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/18 01:36:44 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char **get_position_half(int i, int *lenght, char **str, int *indexs_rd, char *cmd)
{
	int j;
	int d;
	int y;

	j = 0;
	d = 0;
    while (j < i)
    {
        y = 0;
        while (y < lenght[j])
        {
            str[j][y] = cmd[d];
            y++;
            d++;
        }
        str[j][y] = '\0';
        d = indexs_rd[j] ;
        j++;
    }
    str[j] = NULL;
	return (str);
}


char **get_position(int *indexs_rd, int i, char *cmd)
{
    t_cmds *data;
    int		j;
    char	**str;
    int		*lenght;

	j = 0;
	str = (char **)malloc(sizeof(char *) * ( i + 1));
	lenght = (int *)malloc(sizeof(int) * i);
    data = init_stuct();
    lenght[0] = indexs_rd[0] + 1;
    while (j < i)
    {
        if (j > 0)
            lenght[j] = indexs_rd[j] - indexs_rd[j - 1];
        str[j] = (char *)malloc(sizeof(char) * 1024);
        j++;
    }
	str = get_position_half(i, lenght, str, indexs_rd, cmd);
	
    return str;
}