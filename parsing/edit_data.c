/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 09:45:30 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/18 05:09:31 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

t_cmds *data_if_not_red(t_cmds *test)
{
	test->type = get_type_cmnd(test->command);
	test->arguments = get_args(test->command);
	test->redrctions = NULL;
	return (test);
}

int *get_indexs_rd(t_cmds *test, int *indexs_rd, int *i)
{
	int k = 0;
	while (test->command[k] != '\0')
	{
		if (test->command[k] == '"')
				k = skip_d_sqts(++k, test->command); 
		if (test->command[k] == '\'')
				k = skip_s_sqts(++k, test->command);
		if (test->command[k] == '>' || test->command[k] == '<')
		{
			indexs_rd[(*i)] = k;
			(*i)++;
			while (test->command[k + 1] && (test->command[k + 1] == '>' || test->command[k + 1] == '<'))
				k++;
		}
		k++;
	}
	indexs_rd[(*i)] = k;
	(*i)++;
	return (indexs_rd);
}

t_filerdr *init_header_list_rd(t_filerdr *org_list, int p, char **str)
{
	t_filerdr *add_tmp;

	add_tmp = org_list;
	if(p == 1)
	{
		add_tmp->all = str[p];
		add_tmp->name = get_filename(str[p]);
		add_tmp->type = get_type_red(str[p]);
		add_tmp->next_one = NULL;
	}
	return (org_list);
}

t_cmds *initial_red_struct(t_cmds *test, char **str, int p)
{
	t_filerdr *add_tmp;
	t_filerdr *x;

	test->redrctions = (t_filerdr *)malloc(sizeof(t_filerdr));

	add_tmp = init_header_list_rd(test->redrctions,  p, str);
	
	while (str[++p])
	{
		x = (t_filerdr *)malloc(sizeof(t_filerdr));
		x->all = str[p];
		x->name = get_filename(str[p]);
		x->type = get_type_red(str[p]);
		
		add_tmp->next_one = x;
		
		x->next_one = NULL;
		add_tmp = add_tmp->next_one;
	}
	return (test);
}

t_cmds *data_if_red_exist(t_cmds *test, int *ret_value)
{
	char **str;
	int i;
	int *indexs_rd;
	int p;
	int red_len;

	red_len = ( get_lenght_by_char(test->command, '>') + get_lenght_by_char(test->command, '<') );
	p = 1;
	i = 0;
	indexs_rd = (int *)malloc(sizeof(int ) * (red_len + 1));
	// indexs_rd = (int *)malloc(sizeof(int ) * (100 + 1));
	indexs_rd = get_indexs_rd(test, indexs_rd, &i);
	if (i > 1)
	{
		str = get_position(indexs_rd, i, test->command);
		if (syntax_dyal_red(str) == -1)
			(*ret_value) = -1;
		else
		{
			test->type = get_type_cmnd_ifred(test->command, str, i); // get type
			test->arguments = get_args_ifred(test->command, str, i);
			test = initial_red_struct(test, str, p);
		}
		
	}
	return (test);
}
char **get_all_args(char **args, char *type)
{
	t_cmds *data;
	char **tmp;
	int i;

	i = 0;
	data = init_stuct();
	tmp = (char **)malloc(sizeof(char *) * 1);
	tmp[0] = NULL;
	tmp = join2d(tmp, type);
	while(args[i])
	{
		tmp = join2d(tmp, args[i]);
		i++;
	}
	return (tmp);
}
int edit_cmds()
{
	t_cmds *data;
	t_cmds *test;
	int ret_value;

	ret_value = 0;
	char **str;
	data = init_stuct();
	test = data;
	while (test != NULL)
	{
		if (if_redrction(test->command) == 1)
		{
			test = data_if_red_exist(test, &ret_value);
			if (ret_value == -1)
				return -1;
		}
		else
			test = data_if_not_red(test);
		test->all = get_all_args(test->arguments, test->type);
		test = test->next_cmd;
	}
	return (0);
}
