/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syntax_check.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 08:45:11 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/18 04:30:58 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"


int check_syntax(char *s)
{
    int i;
    
    i = 0;
    while (s[i] && s[i] == ' ')
        i++;
    s = delete_backspace((s + i));
    if (strlen(s) == 0)
	{
		// free(s);
		// s = NULL;
        return (1);
	}
	// free(s);
    // s = NULL;
	return 0;
}

int syntax_pipes(char **splitline)
{
    t_cmds *data;
    int j;
	int len;

	j = 0;
    data = init_stuct();
    while (splitline[j])
    {
        len = check_syntax(splitline[j]);
        if (len == 1)
        {
            printf("syntax error near unexpected token 2\n");
            return (-1);
        }
        j++;
    }
    return (0);
}

int syntax_s_sqts(int *i, char *s)
{
    int j;

	j = 0;
    (*i)++;
    while (s[*i])
    {
        if (s[*i] == '\'')
                return (1);
        (*i)++;
    }
    return (0);
}

int syntax_d_sqts(int *i, char *s)
{
    int j;

	j = 0;
    (*i)++;
    while (s[*i])
    {
		
        if (s[*i] == '\"')
		{
			// (*i)++;
			return (1);
		}
        (*i)++;
    }
    return (0);
}

int syntax_quotes_half(int *i)
{
	t_cmds *data;

	data = init_stuct();
	if (data->line[(*i)] == '"')
	{
		if (syntax_d_sqts(i, data->line) == 0)
		{
			printf("syntax error : not in subject \n");
			return (-1);
		}
	}
	else if (data->line[(*i)] == '\'')
	{
		if (syntax_s_sqts(i, data->line) == 0)
		{
			printf("syntax error : not in subject \n");
			return (-1);
		}
	}
	return (0);
}

int syntax_quotes()
{
    t_cmds *data;
	int i;

	i = 0;
    data = init_stuct();
    while (data->line[i])
    {
		if (syntax_quotes_half(&i) == -1)
			return (-1);
        i++;
    }
    return (0);
}

int syntax_errors(char **splitline, int type)
{
    if (type == 1)
        return (syntax_pipes(splitline));
    else if (type == 2)
        return (syntax_quotes());
    return (0);
}

int check_emptyline()
{
    t_cmds *data;
	int i;

    data = init_stuct();
    i = 0;
    while(data->line[i] && data->line[i] == ' ')
        i++;
    if (data->line[i] == '\0')
        return (1);
    return (0);
}