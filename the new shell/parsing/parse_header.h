/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_header.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 07:59:30 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 12:13:52 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSE_HEADER_H
#define PARSE_HEADER_H

#include "../header.h"


int ifsplitis(char *s);
int skip_s_sqts(int i, char *s);
int skip_d_sqts(int i, char *s);
char **more_splt(int *indexs, int j);
char **split_one_cmnd();
char **split_line_pipe(char *s);
int if_redrction(char *s);
// int isthere_redtn(char *str);
// void looking_red(char *str);
// char* split_red(int i, char *s);
char **edited_Split(char *str, char c);
char **edited_Split_typecmd(char *str, char c);
int len_wrds(char *str, char c);
char *delete_backspace(char *s);
char *strjoin1(char *s, char c);
char** get_args(char *line);
char** get_args_ifred(char *line, char **str, int j);
int len_array(char **args);
char **join2d(char **args, char *s);
char** get_type_cmnd(char *line);
char* get_type_cmnd_ifred(char *line, char **str, int nbr);
char *charjoin(char *s, char c);
char **get_position(int *indexs_rd, int i, char *cmd);
void check_if_env(char *var, char **envs);
char **change_env(char **str);
char *addqouts(char *s);
int get_lenght_by_char(char *s, char c);
int check_syntax(char *s);
int syntax_pipes(char **splitline);
int syntax_s_sqts(int *i, char *s);
int syntax_d_sqts(int *i, char *s);
int syntax_quotes();
int syntax_errors(char **splitline, int type);
int check_emptyline();
int syntax_len_wrds(char *str, char c);
int syntax_dyal_red(char **str);
void free_array(char **str);
char* get_filename(char *str);
int get_type_red(char *str);
int edit_cmds();
char **edited_Split_typecmd(char *str, char c);
char **edited_Split_typecmd_half(char *str, char c, int i, char **ret, int j);
char *edited_Split_typecmd_half_dbl_qots(int *i, int j, char *str, char **ret, char c);
char **alloc_ret_split(char **ret, char *str, char c);
char **ft_split_plus(char *str, char c);



//libft
char	*ft_substr(char const *s, unsigned int start, size_t len);
int		ft_isalpha(int c);
char	*ft_strcpy(char *dest, const char *src);
char	*ft_strjoin(char const *s1, char const *s2);
int	ft_isprint(int c);
int		ft_isalnum(int c);
void printf_output();
// char			*ft_strtrim(char const *s); //old one
char			*ft_strtrim(char const *s1, char const *set); // new my libtf
#endif