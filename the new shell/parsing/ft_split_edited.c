/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_edited.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rel-bour <rel-bour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 09:38:43 by rel-bour          #+#    #+#             */
/*   Updated: 2021/11/21 10:50:49 by rel-bour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header.h"

char *strjoin1(char *s, char c)
{
    int i;
    char *str;

    i = 0;
    int len = strlen(s);
    str = (char *)malloc(sizeof(char *) * (len + 2));
    while (s[i] != '\0')
    {
        str[i] = s[i];
        i++;
    }
    str[i] = c;
    str[i + 1] = '\0';
    return str;
}

char *delete_backspace(char *s)
{
    int len;
    char *ret;

    len = (strlen(s));
    if (len > 0 &&  s[len - 1] == ' ')
    {
        len = len - 1;
        while (len >= 0 &&  s[len] == ' ')
            len--;
        len = len + 1;
        ret = (char *)malloc(sizeof(char) * (len + 1));
        int j = 0;
        while (j < (len))
        {
            ret[j] = s[j];
            j++;
        }
        ret[j] = '\0';
    }
    else
	{
		char *tmp = strdup(s);
		// free(s);
        return (strdup(s));
	}
    return (ret);
}

int len_wrds(char *str, char c)
{
    int i;
    int len;

    i = 0;
    len = 0;
	while (str[i] && str[i] == c)
		i++;
	// char *tmp = (str + 1);
    str = delete_backspace(str + i);        /// hnaa fin wsalt m3a 5 tsba7 knt kankhwi leaak
	// free(tmp);
	i = 0;
    while (str[i])
    {
		if (str[i] == '\"')
			i = skip_d_sqts(++i, str); 
		else if (str[i] == '\'')
			i = skip_s_sqts(++i, str); 
		else if (str[i] == c) /// char c
		{
			len = len + 1;
			while (str[i] && str[i] == c)
				i++;
			continue;
		}
		i++;
    }
	// free(str);
	// str = NULL;
    return (len);
}

char **org_alloc_ret_split(char **ret, char *str, char c)
{ 
    int i;
    int len;

    i = 0;
    len = len_wrds(str, c);
    ret = (char **)malloc(sizeof(char *) * (len + 2));
    while (i < len + 1)
    {
        ret[i] = (char *)malloc(sizeof(char) * 100);
        i++;
    }
    ret[i] = NULL;
    return (ret);
}

char *org_typecmd_half_dbl_qots(int *i, int j, char *str, char **ret, char c)
{
	(*i)++;
	while (str[*i] && str[*i] != c)
	{
		ret[j] =  strjoin1(ret[j], str[*i]);
		(*i)++;
	}
	(*i)++;
	return (ret[j]);
}

char **org_Split_typecmd_half(char *str, char c, int i, char **ret, int j)
{
	while (str[i] != '\0')
    {
		if (str[i] == '\"')
			ret[j] = org_typecmd_half_dbl_qots(&i, j,str, ret, '\"');
		else if (str[i] == '\'')
			ret[j] = org_typecmd_half_dbl_qots(&i, j,str, ret, '\'');
		else if (str[i] == c)  /// char c
		{
			i++;
			while (str[i] && str[i] == ' ')
				i++;
			j++;
			ret[j] = strdup("");
		}
		else
		{
			ret[j] =  strjoin1(ret[j], str[i]);
			i++;
		}
    }
	return (ret);
}

char **edited_Split(char *str, char c)
{
    int i;
	int j;
    char **ret;

	ret = org_alloc_ret_split(ret, str, c);
    i = 0;
    j = 0;
    ret[j] = strdup("");
    str = delete_backspace(str);
    while (str && str[i] == ' ')
        i++;
	return (org_Split_typecmd_half(str, c, i, ret, j));
}
